USE classic_models;

SELECT customerName FROM customers WHERE country = "Philippines";

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

SELECT lastName, firstName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

SELECT customerName FROM customers WHERE state IS NULL;

SELECT lastName, firstName, email FROM employees WHERE employeeNumber = 1216;

SELECT customerName, country, creditLimit FROM customers WHERE NOT country = "USA" AND creditLimit >  3000;

SELECT customerName FROM customers WHERE NOT customerName LIKE "%a%";

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

SELECT*FROM productlines WHERE textDescription LIKE "%state of the art%";

SELECT DISTINCT Country FROM customers;

SELECT DISTINCT status FROM orders;

SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

SELECT a.city AS city, b.officeCode AS officeCode, b.firstName AS employee_firstName, b.lastName AS employee_lastName FROM offices AS a JOIN employees AS b  ON a.officeCode = b.officeCode WHERE city = "Tokyo";

SELECT b.customerName AS customerName FROM employees AS a JOIN customers AS b  ON a.employeeNumber = b.salesRepEmployeeNumber WHERE employeeNumber = 1611;

SELECT p.productName, c.customerName FROM customers AS c JOIN orders AS o ON c.customerNumber = o.customerNumber JOIN orderdetails AS d ON o.orderNumber = d.orderNumber JOIN products as p ON d.productCode = p.productCode WHERE c.customerName = "Baane Mini Imports"; 

SELECT a.firstName, a.lastName, b.customerName, c.country FROM payments AS p JOIN customers AS b ON p.customerNumber = b.customerNumber JOIN employees AS a ON b.salesRepEmployeeNumber = a.employeeNumber JOIN offices as c ON a.officeCode = c.officeCode WHERE b.country = c.country;

SELECT lastName, firstName FROM employees WHERE reportsTo = 1143;

SELECT productName, MAX(MSRP) FROM products;

SELECT COUNT(customerNumber) FROM customers WHERE country = "UK" 

SELECT productline, COUNT(*) FROM products GROUP BY productline;

SELECT a.lastname, a.firstname, COUNT(*)  FROM employees AS a JOIN customers AS b ON a.employeeNumber = b.salesRepEmployeeNumber GROUP BY b.salesRepEmployeeNumber;

SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;







